package com.example.service;



import com.example.model.User;

import java.util.List;

public interface UserService {

	User findById(Long id);

	User findByName(String name);

	User findByLoginAndPassword(String login, String password);

	void saveUser(User user);

	void updateUser(User user);

	void deleteUserById(Long id);

	void deleteAllUsers();

	List<User> findAllUsers();

	boolean isUserExist(User user);
}
