(function () {
    'use strict';

    angular
        .module('app')
        .controller('Client', Client);

    Client.$inject = ['$scope'];

    function Client($scope) {

  $scope.todos = [
    {text:'Pizza1', done:false},
    {text: 'Pizza2', done:false}
  ];

  $scope.getTotalTodos = function () {
    return $scope.todos.length;
  };


  $scope.addTodo = function () {
    $scope.todos.push({text:$scope.formTodoText, done:false});
    $scope.formTodoText = '';
  };

    $scope.clearCompleted = function () {
        $scope.todos = _.filter($scope.todos, function(todo){
            return !todo.done;
        });
    };

    }

})();
